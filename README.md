# hdd_performance_model

Because of a discussion on the zfs mailing list, and the fact that I'm taking a 
class on modeling right now, I thought it would be fun to write a model of HDD 
performance, to estimate the performance benefit for write-back buffering random 
writes (or re-ordering random reads).

Results: About 2.2x faster, regardless of hard drive size, assuming 
`num_stops > ~50`.