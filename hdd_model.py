#!/usr/bin/env python3
import random


max_lba = 1000000
num_stops = 100
full_seek = 12.0  # avg seek 6ms, full head sweep 12ms
avg_latency = 3.0  # 10krpm -> 6ms full rotation, so avg rotational latency = 3ms


def time_naive(stops):  # type: (list) -> float
    current_pos = 0
    total_time = 0.0

    for stop in stops:
        seek_distance = abs(stop - current_pos)
        total_time += (float(seek_distance) / float(max_lba)) * full_seek
        total_time += avg_latency
        current_pos = stop  # the head stays at that position after seeking there

    return total_time


def time_elevator(stops):  # type: (list) -> float
    stops_sorted = sorted(stops)
    return time_naive(stops_sorted)


def main():
    random.seed(4)  # arbitrary number, so results are reproducible

    stops = []
    for i in range(num_stops):
        stops.append(random.randrange(max_lba))

    print("time_naive: {}".format(time_naive(stops)))
    print("time_elevator: {}".format(time_elevator(stops)))


if __name__ == '__main__':
    main()
